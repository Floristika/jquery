// Оновлення функції fetchAlbums з використанням jQuery
function fetchAlbums() {
    $.get('https://jsonplaceholder.typicode.com/albums', function(albums) {
        const albumsContainer = $('#albums');
        albumsContainer.empty();
        albums.forEach(function(album) {
            const albumDiv = $('<div>').addClass('album').text(album.title).data('albumId', album.id);
            albumDiv.on('click', function() {
                fetchPhotos(album.id);
            });
            albumsContainer.append(albumDiv);
        });
    }).fail(function(error) {
        console.error('Error fetching albums:', error);
    });
}

// Оновлення функції fetchPhotos з використанням jQuery
function fetchPhotos(albumId) {
    $.get(`https://jsonplaceholder.typicode.com/photos?albumId=${albumId}`, function(photos) {
        const photosContainer = $('#photos');
        photosContainer.empty();
        photos.forEach(function(photo) {
            const photoDiv = $('<div>').addClass('photo');
            $('<h3>').text(photo.title).appendTo(photoDiv);
            $('<img>').attr({ src: photo.thumbnailUrl, alt: photo.title }).appendTo(photoDiv);
            photosContainer.append(photoDiv);
        });
    }).fail(function(error) {
        console.error('Error fetching photos:', error);
    });
}

// Fetch albums when the page loads
$(document).ready(function() {
    fetchAlbums();
});
